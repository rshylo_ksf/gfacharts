package com.example.rshylo.gfapopupcharts.entries;

import com.example.rshylo.gfapopupcharts.defaults.DefaultXAxis;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;

/**
 * Created by Roman Shylo on 8/18/2017.
 * whoose.daddy@gmail.com
 */

public class DataSet {

    private Entry[] entries;
    private int[] colors;
    private IEntryRenderer entryRenderer;
    private IAxis xAxis;

    private int maxEntryIndex;

    public DataSet(Entry[] entries, IEntryRenderer entryRenderer) {
        this(entries, entryRenderer, null);
    }

    public DataSet(Entry[] entries, IEntryRenderer entryRenderer, int[] colors) {
        this.entries = entries;
        this.entryRenderer = entryRenderer;
        this.colors = colors;
        init();
    }

    ///////////////////////////////////////////////////////////////////////////
    // SETUP
    ///////////////////////////////////////////////////////////////////////////

    private void init() {
        // Find max value
        float tempMax = 0f;
        for (int i = 0; i < getEntries().length; i++) {
            if (getEntries()[i].getValuesSum() > tempMax) {
                tempMax = getEntries()[i].getValuesSum();
                maxEntryIndex = i;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // GETTERS
    ///////////////////////////////////////////////////////////////////////////

    public Entry[] getEntries() {
        return entries != null ? entries : new Entry[0];
    }

    public int[] getColors() {
        return colors;
    }

    public IEntryRenderer getEntryRenderer() {
        return entryRenderer;
    }

    public float getMaxValue() {
        return getEntries()[maxEntryIndex].getValuesSum();
    }

    public IAxis getXAxis() {
        if (xAxis == null) {
            xAxis = new DefaultXAxis();
        }
        return xAxis;
    }

    public void setXAxis(IAxis xAxis) {
        this.xAxis = xAxis;
    }
}

package com.example.rshylo.gfapopupcharts.entries;

/**
 * Created by Roman Shylo on 8/23/2017.
 * whoose.daddy@gmail.com
 */

public enum WidthMode {

    NORMAL(0),
    FIT_TO_WIDTH(1);

    int id;

    WidthMode(int id) {
        this.id = id;
    }

    public static WidthMode fromId(int id) {
        for (WidthMode f : values()) {
            if (f.id == id) {
                return f;
            }
        }
        throw new IllegalArgumentException();
    }
}

package com.example.rshylo.gfapopupcharts.custom;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.defaults.DefaultEntryValuesRenderer;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.formatters.IEntryValueFormatter;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public class CustomEntryValuesRenderer extends DefaultEntryValuesRenderer {

    private float largeTextSize;

    @Override
    protected void setup() {
        largeTextSize = ChartUtils.convertDpToPixel(TEXT_SIZE_VALUE + 2f);
        super.setup();
    }

    @Override
    public void renderEntryValue(Canvas c, RectF entryRect, RectF stackRect, Entry e, int stackIndex, IEntryValueFormatter formatter) {
        boolean largeText = e.getValues()[stackIndex] < 1f;
        valueTextPaint.setTextSize(largeText ? largeTextSize : textSize);
        super.renderEntryValue(c, entryRect, stackRect, e, stackIndex, formatter);
    }

    /*@Override
    public RectF getRectToDrawEntryValue(RectF stackRect, Entry e, int stackIndex, String text) {
        float value = e.getValues()[stackIndex];
        RectF rectF = new RectF(stackRect);
        boolean drawAbove = value < 1f;
        if (drawAbove) {
            float size = Utils.calcTextHeight(textPaint, text) / 2f;
            rectF.top = rectF.top - size - Utils.convertDpToPixel(1f);
            rectF.bottom = rectF.top;
            return rectF;
        }
        return super.getRectToDrawEntryValue(stackRect, e, stackIndex, text);
    }*/

}

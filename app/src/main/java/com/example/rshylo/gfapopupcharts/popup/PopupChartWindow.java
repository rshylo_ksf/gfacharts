package com.example.rshylo.gfapopupcharts.popup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.example.rshylo.gfapopupcharts.R;
import com.example.rshylo.gfapopupcharts.charts.ChartView;
import com.example.rshylo.gfapopupcharts.entries.DataSet;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public class PopupChartWindow extends PopupWindow implements DraggableOnTouchListener.IOnPositionChangedListener {

    private static final String TAG = "PopupChartWindow";

    private ChartView chartView;

    private DataSet dataSet;

    private DraggableOnTouchListener onTouchListener;

    public PopupChartWindow(Context context, DataSet dataSet) {
        super(context);
        this.dataSet = dataSet;
        View view = createView(context);
        setContentView(view);
        init(context, view);
        fillData();
    }

    private static View createView(Context context) {
        return LayoutInflater.from(context).inflate(R.layout.view_popup_chart_window, null);
    }

    private void init(Context context, View view) {
        setAnimationStyle(R.style.PopUpAnimation);
        setClippingEnabled(false);
        setOutsideTouchable(true);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        onTouchListener = new DraggableOnTouchListener(this);
        view.setOnTouchListener(onTouchListener);
        setWindowSizes(context);
        // Bind views
        chartView = view.findViewById(R.id.chart);
    }

    private void setWindowSizes(Context context) {
        setWidth(PopupSizeUtils.getLayoutWidth(context));
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        onTouchListener.forcePositions(-PopupSizeUtils.getLandscapeSoftBarFix(context), 0);
    }

    public void show(Activity activity) {
        showAtLocation(activity.findViewById(android.R.id.content), Gravity.CENTER, 0, 0);
    }

    @Override
    public void onPositionChanged(int newX, int newY) {
        updateLocationOnScreen(newX, newY);
    }

    private void updateLocationOnScreen(int x, int y) {
        update(x, y, getWidth(), getHeight());
    }

    public void onConfigChanges(Context context) {
        setWindowSizes(context);
    }

    private void fillData() {
        chartView.setDataSet(dataSet);
    }

}

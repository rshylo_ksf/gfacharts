package com.example.rshylo.gfapopupcharts.renderers;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.entries.IAxis;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPort;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public interface IAxisRenderer {

    RectF renderAxis(Canvas c, RectF rect, Entry[] entries, IChartViewPort viewPort,
                     IAxis axis, IEntryRenderer entryRenderer);
}

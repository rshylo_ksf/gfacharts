package com.example.rshylo.gfapopupcharts.custom;

import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.formatters.IEntryValueFormatter;

import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by Roman Shylo on 8/25/2017.
 * whoose.daddy@gmail.com
 */

public class CustomEntryValueFormatter implements IEntryValueFormatter {

    private DecimalFormat decimalFormat;

    public CustomEntryValueFormatter() {
        decimalFormat = new DecimalFormat("#.##");
    }

    @Override
    public CharSequence format(Entry entry, int stackIndex) {
        float value = entry.getValues()[stackIndex];
        return format(value);
    }

    @Override
    public CharSequence formatTotalValue(Entry entry) {
        return format(entry.getValuesSum());
    }

    private CharSequence format(float value) {
        String text = String.format(Locale.US, "$%s", decimalFormat.format(value));
        SpannableString spannable = new SpannableString(text);
        spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                0, spannable.length(), 0);
        return spannable;
    }

}

package com.example.rshylo.gfapopupcharts.formatters;

import com.example.rshylo.gfapopupcharts.entries.Entry;

/**
 * Created by Roman Shylo on 8/18/2017.
 * whoose.daddy@gmail.com
 */

public interface IEntryValueFormatter {

    CharSequence format(Entry entry, int stackIndex);

    CharSequence formatTotalValue(Entry entry);

}

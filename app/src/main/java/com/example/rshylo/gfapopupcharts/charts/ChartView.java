package com.example.rshylo.gfapopupcharts.charts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.rshylo.gfapopupcharts.R;
import com.example.rshylo.gfapopupcharts.custom.CustomEntryValuesRenderer;
import com.example.rshylo.gfapopupcharts.defaults.DefaultEntryRenderer;
import com.example.rshylo.gfapopupcharts.defaults.EmptyDataSet;
import com.example.rshylo.gfapopupcharts.demo.DemoDataSet;
import com.example.rshylo.gfapopupcharts.entries.DataSet;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.entries.IAxis;
import com.example.rshylo.gfapopupcharts.entries.WidthMode;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPort;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPortOwner;

/**
 * Created by Roman Shylo on 8/18/2017.
 * whoose.daddy@gmail.com
 */

public class ChartView extends View implements IChartViewPortOwner {

    private final static boolean DRAW_DEMO = false;

    public ChartView(Context context) {
        this(context, null);
    }

    public ChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        if (attrs != null) {
            parseAttrs(attrs);
        }
    }

    private ChartViewPort viewPort = new ChartViewPort();
    private DataSet dataSet;

    private void init() {
        if (DRAW_DEMO || isInEditMode()) {
            setEditModeDefaults();
        }
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    private void parseAttrs(AttributeSet attrs) {
        TypedArray a = getContext().getTheme()
                                   .obtainStyledAttributes(attrs, R.styleable.ChartView, 0, 0);
        float entryWidth = a.getDimensionPixelSize(R.styleable.ChartView_entry_width, 0);
        viewPort.setEntryW(entryWidth);
        float entrySpace = a.getDimensionPixelSize(R.styleable.ChartView_entry_space, -1);
        viewPort.setEntrySpace(entrySpace);
        int widthModeValue = a.getInt(R.styleable.ChartView_width_mode, 0);
        viewPort.setWidthMode(WidthMode.fromId(widthModeValue));
        a.recycle();
    }

    private void setEditModeDefaults() {
        dataSet = new DemoDataSet(new DefaultEntryRenderer(new CustomEntryValuesRenderer()));
        dataSet.getEntryRenderer().setChart(this);
    }

    public void setDataSet(@Nullable DataSet dataSet) {
        if (dataSet != null) {
            dataSet.getEntryRenderer().setChart(this);
        }
        this.dataSet = dataSet;
        requestLayout();
    }

    public DataSet getDataSet() {
        if (dataSet == null) {
            dataSet = new EmptyDataSet();
        }
        return dataSet;
    }

    public IAxis getXAxis() {
        return getDataSet().getXAxis();
    }

    ///////////////////////////////////////////////////////////////////////////
    // INNER
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        viewPort.setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        viewPort.setOriginalDimens(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
        viewPort.renderSize(getDataSet().getEntryRenderer(), getDataSet().getEntries().length);

        widthMeasureSpec = MeasureSpec.makeMeasureSpec(viewPort.getWidth(), MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawData(canvas);
    }

    private void drawData(Canvas c) {
        RectF contentRect = viewPort.getContentArea();
        // Draw X Axis
        RectF xAxisRect = new RectF(contentRect.left, contentRect.bottom - viewPort.getXAxisHeight(),
                contentRect.right, contentRect.bottom);
        xAxisRect = getXAxis().getAxisRenderer().renderAxis(c, xAxisRect,
                getDataSet().getEntries(), viewPort, getXAxis(), getDataSet().getEntryRenderer());
        // Draw entries
        RectF chartRect = new RectF(xAxisRect.left, contentRect.top, xAxisRect.right, xAxisRect.top);
        float pointYSize = (chartRect.bottom - chartRect.top) / dataSet.getMaxValue();

        float entryWidth = viewPort.getEntryW(getDataSet().getEntryRenderer());
        RectF cellRect = new RectF(chartRect.left, chartRect.top,
                chartRect.left + entryWidth, chartRect.bottom);
        for (Entry entry : getDataSet().getEntries()) {
            cellRect = getDataSet().getEntryRenderer().renderEntry(c, cellRect, entry,
                    pointYSize, getDataSet().getColors());
            // Calc next entry position
            float lastRight = cellRect.right;
            cellRect.left = lastRight + viewPort.getEntrySpace();
            cellRect.right = cellRect.left + entryWidth;

        }
    }

    @Override
    public IChartViewPort getViewPort() {
        return viewPort;
    }
}

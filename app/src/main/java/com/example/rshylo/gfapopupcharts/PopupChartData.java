package com.example.rshylo.gfapopupcharts;

import java.util.Map;

/**
 * Created by Roman Shylo on 8/25/2017.
 * whoose.daddy@gmail.com
 */

public class PopupChartData {

    private String title;
    private String format;
    private Map<String, String> valuesMap;

    public PopupChartData(String title, String format, Map<String, String> valuesMap) {
        this.title = title;
        this.format = format;
        this.valuesMap = valuesMap;
    }

    public String getTitle() {
        return title;
    }

    public String getFormat() {
        return format;
    }

    public Map<String, String> getValuesMap() {
        return valuesMap;
    }
}

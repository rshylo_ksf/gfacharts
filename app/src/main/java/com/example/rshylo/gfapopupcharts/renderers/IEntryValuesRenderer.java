package com.example.rshylo.gfapopupcharts.renderers;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.formatters.IEntryValueFormatter;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public abstract class IEntryValuesRenderer {

    private IEntryValueFormatter valueFormatter;

    private int textColor;
    private int textTotalColor;
    private float textSize;
    private boolean drawTextShadow;

    public IEntryValuesRenderer(IEntryValueFormatter valueFormatter) {
        this.valueFormatter = valueFormatter;
    }

    public final void renderEntryValue(Canvas c, RectF entryRect, RectF stackRect,
                                       Entry e, int stackIndex) {
        this.renderEntryValue(c, entryRect, stackRect, e, stackIndex, valueFormatter);
    }

    public final void renderEntryTotalValue(Canvas c, RectF entryRect, Entry entry) {
        this.renderEntryTotalValue(c, entryRect, entry, valueFormatter);
    }

    protected abstract void renderEntryValue(Canvas c, RectF entryRect, RectF stackRect,
                                             Entry e, int stackIndex, IEntryValueFormatter valueFormatter);

    public abstract void renderEntryTotalValue(Canvas c, RectF entryRect, Entry entry,
                                               IEntryValueFormatter valueFormatter);

    ///////////////////////////////////////////////////////////////////////////
    // GETTERS / SETTERS
    ///////////////////////////////////////////////////////////////////////////

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getTextTotalColor() {
        return textTotalColor;
    }

    public void setTextTotalColor(int textTotalColor) {
        this.textTotalColor = textTotalColor;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public boolean isDrawTextShadow() {
        return drawTextShadow;
    }

    public void setDrawTextShadow(boolean drawTextShadow) {
        this.drawTextShadow = drawTextShadow;
    }

}

package com.example.rshylo.gfapopupcharts.entries;

import android.support.annotation.Nullable;

import com.example.rshylo.gfapopupcharts.formatters.IEntryColorFormatter;

/**
 * Created by Roman Shylo on 8/18/2017.
 * whoose.daddy@gmail.com
 */

public class Entry {

    private float[] values;
    private String label;
    private @Nullable IEntryColorFormatter colorFormatter;
    private Object tag;

    private int stackSize;
    private float valuesSum;

    public Entry(float[] values, String label) {
        this(values, label, null);
    }

    public Entry(float[] values, String label, @Nullable IEntryColorFormatter colorFormatter) {
        this.values = values;
        this.label = label;
        this.colorFormatter = colorFormatter;
        calcInner();
    }

    private void calcInner() {
        stackSize = values.length;
        // Calc sum
        for (float v : getValues()) {
            valuesSum += v;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // GETTERS / SETTERS
    ///////////////////////////////////////////////////////////////////////////

    public float[] getValues() {
        return values;
    }

    public String getLabel() {
        return label;
    }

    public int getColorFor(int valueIndex) {
        return colorFormatter != null ? colorFormatter.formatColor(this, valueIndex) : -1;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public int getStackSize() {
        return stackSize;
    }

    public float getValuesSum() {
        return valuesSum;
    }
}

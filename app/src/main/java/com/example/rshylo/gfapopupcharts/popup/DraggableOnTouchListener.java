package com.example.rshylo.gfapopupcharts.popup;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public class DraggableOnTouchListener implements View.OnTouchListener {

    private static final String TAG = "DraggableOnTouchListene";

    private @NonNull IOnPositionChangedListener listener;

    private float currentX = -1, currentY = -1;
    private float dx, dy;

    public DraggableOnTouchListener(@NonNull IOnPositionChangedListener listener) {
        this.listener = listener;
    }

    public void forcePositions(int x, int y) {
        currentX = x;
        currentY = y;
        notifyListener(x, y);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        Log.i(TAG, "onTouch: " + event.getAction());
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            dx = currentX - event.getRawX();
            dy = currentY - event.getRawY();
            Log.i(TAG, "onTouch DOWN: " + event.getRawX() + " - " + event.getRawY());
        } else if (action == MotionEvent.ACTION_MOVE || action == MotionEvent.ACTION_UP) {
            int newX = Math.round(event.getRawX() + dx);
            int newY = Math.round(event.getRawY() + dy);
            Log.i(TAG, "onTouch MOVE: " + event.getRawX() + " - " + event.getRawY());
            if (newX != currentX || newY != currentY) {
                currentX = newX;
                currentY = newY;
                notifyListener(Math.round(currentX), Math.round(currentY));
            }
        }
        return true;
    }

    private void notifyListener(int x, int y) {
        listener.onPositionChanged(x, y);
    }

    public interface IOnPositionChangedListener {
        void onPositionChanged(int newX, int newY);
    }
}

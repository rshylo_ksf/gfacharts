package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.formatters.IEntryValueFormatter;
import com.example.rshylo.gfapopupcharts.renderers.IEntryValuesRenderer;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

@SuppressWarnings("WeakerAccess")
public class DefaultEntryValuesRenderer extends IEntryValuesRenderer {

    public static final float TEXT_SIZE_VALUE = 12f;

    protected Paint textPaint;
    protected TextPaint valueTextPaint;
    protected TextPaint valueTotalTextPaint;

    protected float textSize;

    public DefaultEntryValuesRenderer() {
        this(new DefaultEntryValueFormatter());
    }

    public DefaultEntryValuesRenderer(IEntryValueFormatter valueFormatter) {
        super(valueFormatter);
        setup();
        init();
    }

    protected void setup() {
        textSize = ChartUtils.convertDpToPixel(TEXT_SIZE_VALUE);
        setTextSize(textSize);
        setTextColor(Color.WHITE);
        setTextTotalColor(Color.BLACK);
        setDrawTextShadow(false);
    }

    private void init() {
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        if (isDrawTextShadow()) {
            textPaint.setShadowLayer(12.0f, 0f, 0f, Color.parseColor("#55000000"));
        }
        textPaint.setTextSize(getTextSize());
        textPaint.setColor(getTextColor());
        valueTextPaint = new TextPaint(textPaint);
        //
        valueTotalTextPaint = new TextPaint(valueTextPaint);
        valueTotalTextPaint.setColor(getTextTotalColor());
    }

    @Override
    public void renderEntryValue(Canvas c, RectF entryRect, RectF stackRect, Entry e, int stackIndex, IEntryValueFormatter formatter) {
        CharSequence text = formatter.format(e, stackIndex);
        ChartUtils.drawTextInRect(c, valueTextPaint,
                getRectToDrawEntryValue(stackRect, e, stackIndex, text),
                text);
    }

    @Override
    public void renderEntryTotalValue(Canvas c, RectF entryRect, Entry entry, IEntryValueFormatter valueFormatter) {
        CharSequence label = valueFormatter.formatTotalValue(entry);
        ChartUtils.drawTextInRect(c, valueTotalTextPaint,
                getRectToDrawTotalEntryValue(entryRect, entry, label),
                label);
    }

    public RectF getRectToDrawTotalEntryValue(RectF entryRect, Entry e, CharSequence text) {
        int labelH = ChartUtils.calcTextSize(textPaint, text, entryRect).y;
        // Add offset to move value a bit to top
        float offset = ChartUtils.convertDpToPixel(2f);
        return new RectF(entryRect.left, entryRect.top - labelH - offset,
                entryRect.right, entryRect.top - offset);
    }

    public RectF getRectToDrawEntryValue(RectF stackRect, Entry e, int stackIndex, CharSequence text) {
        return stackRect;
    }

}

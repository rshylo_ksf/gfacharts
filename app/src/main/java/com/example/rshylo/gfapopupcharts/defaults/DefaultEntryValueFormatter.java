package com.example.rshylo.gfapopupcharts.defaults;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.formatters.IEntryValueFormatter;

/**
 * Created by Roman Shylo on 8/18/2017.
 * whoose.daddy@gmail.com
 */

public class DefaultEntryValueFormatter implements IEntryValueFormatter {

    @Override
    public String format(Entry entry, int stackIndex) {
        float value = entry.getValues()[stackIndex];
        return String.valueOf(value);
    }

    @Override
    public String formatTotalValue(Entry entry) {
        return String.valueOf(entry.getValuesSum());
    }
}

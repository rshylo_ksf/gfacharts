package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Typeface;
import android.support.annotation.IntDef;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.formatters.IAxisValueFormatter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Roman Shylo on 8/18/2017.
 * whoose.daddy@gmail.com
 */

public class DefaultXAxisFormatter implements IAxisValueFormatter {

    @IntDef(flag = true, value = {Typeface.NORMAL, Typeface.BOLD, Typeface.ITALIC, Typeface.BOLD_ITALIC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TextStyle {
    }

    private @TextStyle int textStyle = Typeface.NORMAL;

    public void setTextStyle(@TextStyle int textStyle) {
        this.textStyle = textStyle;
    }

    @Override
    public CharSequence format(Entry entry, int xIndex) {
        String label = entry.getLabel();
        return format(label);
    }

    private CharSequence format(String value) {
        SpannableString spannable = new SpannableString(value);
        spannable.setSpan(new StyleSpan(textStyle),
                0, spannable.length(), 0);
        return spannable;
    }

}

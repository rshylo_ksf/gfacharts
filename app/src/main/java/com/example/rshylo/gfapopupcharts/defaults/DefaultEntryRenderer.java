package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;
import com.example.rshylo.gfapopupcharts.renderers.IEntryValuesRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public class DefaultEntryRenderer extends IEntryRenderer {

    private Paint barPaint;
    private Paint fullEntryBarPaint;
    private RectF rectF;

    private List<RectF> entryRectSet;

    public DefaultEntryRenderer() {
        this(new DefaultEntryValuesRenderer());
    }

    public DefaultEntryRenderer(IEntryValuesRenderer entryValuesRenderer) {
        super(entryValuesRenderer);
        init();
    }

    private void init() {
        setDrawEntryShadow(true);
        //
        barPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        barPaint.setStyle(Paint.Style.FILL);
        //
        fullEntryBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        barPaint.setStyle(Paint.Style.FILL);
        fullEntryBarPaint.setColor(Color.TRANSPARENT);
        fullEntryBarPaint.setShadowLayer(12.0f, 0f, 0f, Color.parseColor("#55000000"));
        rectF = new RectF();
        //
        entryRectSet = new ArrayList<>();
    }

    private RectF shadowRect = new RectF();

    @Override
    public RectF renderEntry(Canvas c, RectF rect, Entry entry, float pointYSize, int[] stackColors, IEntryValuesRenderer valuesRenderer) {
        entryRectSet.clear();
        rectF.set(rect);
        rectF.top = rect.bottom;
        // Draw shadow
        if (isDrawEntryShadow()) {
            shadowRect.set(rect);
            shadowRect.top = shadowRect.bottom - pointYSize * entry.getValuesSum();
            c.drawRect(shadowRect, fullEntryBarPaint);
        }
        //
        for (int i = 0; i < entry.getStackSize(); i++) {
            float barSize = pointYSize * entry.getValues()[i];
            barPaint.setColor(getStackColor(entry, stackColors, i));

            float lastTop = rectF.top;
            rectF.top = lastTop - barSize;
            rectF.bottom = lastTop;
            c.drawRect(rectF, barPaint);
            entryRectSet.add(new RectF(rectF));
        }
        // Save last top Y
        rect.top = rectF.top;
        // Draw values above charts
        for (int i = 0; i < entry.getStackSize(); i++) {
            valuesRenderer.renderEntryValue(c, rect, entryRectSet.get(i), entry, i);
        }
        // Draw total value
        valuesRenderer.renderEntryTotalValue(c, rect, entry);
        return rect;
    }

    private int getStackColor(Entry entry, int[] stackColors, int stackIndex) {
        int color = entry.getColorFor(stackIndex);
        if (color == -1) {
            // If color not defined in entry, try to find it in global dataSet colors
            if (stackColors.length != 0 && stackIndex < stackColors.length) {
                color = stackColors[stackIndex];
            } else {
                // Set default color
                color = Color.LTGRAY;
            }
        }
        return color;
    }

}

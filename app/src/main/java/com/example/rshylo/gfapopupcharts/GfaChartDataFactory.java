package com.example.rshylo.gfapopupcharts;

import android.text.TextUtils;

import com.example.rshylo.gfapopupcharts.entries.Entry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Roman Shylo on 8/23/2017.
 * whoose.daddy@gmail.com
 */

public class GfaChartDataFactory {

    public static Entry[] createEntries(Map<String, String> map) {
        List<Entry> result = new ArrayList<>();
        List<String> tmpValues = new ArrayList<>();
        String tmpTitle = null;
        for (String value : map.values()) {
            if (!TextUtils.isDigitsOnly(value)) {
                // Title
                if (tmpTitle != null) {
                    result.add(createEntry(tmpTitle, tmpValues));
                }
                tmpTitle = value;
                tmpValues.clear();
            } else {
                // Value
                tmpValues.add(value);
            }
        }
        // Save last iteration
        result.add(createEntry(tmpTitle, tmpValues));
        return result.toArray(new Entry[result.size()]);
    }

    private static Entry createEntry(String title, List<String> values) {
        // Convert values
        int size = values.size();
        float[] floatV = new float[size];
        for (int i = 0; i < values.size(); i++) {
            floatV[size - 1 - i] = Float.parseFloat(values.get(i));
        }
        return new Entry(floatV, title);
    }

}

package com.example.rshylo.gfapopupcharts.demo;

import com.example.rshylo.gfapopupcharts.defaults.ColorTemplate;
import com.example.rshylo.gfapopupcharts.entries.DataSet;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public class DemoDataSet extends DataSet {

    public DemoDataSet(IEntryRenderer entryRenderer) {
        super(testEntries(), entryRenderer, ColorTemplate.getGfaChartColors());
    }

    private static Entry[] testEntries() {
        List<Entry> entries = new ArrayList<>();
        entries.add(new Entry(new float[]{113, 398}, "Jan\n2017"));
        entries.add(new Entry(new float[]{138, 303}, "May\n2017"));
        entries.add(new Entry(new float[]{196, 429}, "Jul\n2017"));
        entries.add(new Entry(new float[]{136, 308}, "Aug\n2017"));

        entries.add(new Entry(new float[]{125, 271}, "Jan\n2017"));
        entries.add(new Entry(new float[]{123, 252}, "May\n2017"));
        entries.add(new Entry(new float[]{162, 356}, "Jul\n2017"));
        entries.add(new Entry(new float[]{133, 285}, "Aug\n2017"));

        entries.add(new Entry(new float[]{125, 271}, "Jan\n2017"));
        entries.add(new Entry(new float[]{123, 252}, "May\n2017"));
        return entries.toArray(new Entry[entries.size()]);
    }

}

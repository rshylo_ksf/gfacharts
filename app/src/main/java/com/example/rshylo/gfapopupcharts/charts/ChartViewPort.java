package com.example.rshylo.gfapopupcharts.charts;

import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.entries.WidthMode;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPort;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public class ChartViewPort implements IChartViewPort {

    private WidthMode widthMode;
    private int width;
    private int height;
    private float entryWidth;
    private float entrySpace;

    private int paddingLeft;
    private int paddingTop;
    private int paddingRight;
    private int paddingBottom;

    public ChartViewPort() {
        entryWidth = ChartUtils.convertDpToPixel(48);
        entrySpace = ChartUtils.convertDpToPixel(6);
    }

    public void setOriginalDimens(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setPadding(int left, int top, int right, int bottom) {
        this.paddingLeft = left;
        this.paddingTop = top;
        this.paddingRight = right;
        this.paddingBottom = bottom;
    }

    @Override
    public void renderSize(IEntryRenderer renderer, int entryCount) {
        float totalSpace = (entryCount - 1) * getEntrySpace();
        if (widthMode == WidthMode.NORMAL) {
            float newWidth = entryCount * getEntryW(renderer) + getHorizontalPadding();
            newWidth += totalSpace;
            width = Math.round(newWidth);
        } else if (widthMode == WidthMode.FIT_TO_WIDTH) {
            float availableWidth = width - totalSpace - getHorizontalPadding();
            entryWidth = availableWidth / entryCount;
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setWidthMode(WidthMode mode) {
        widthMode = mode;
    }

    @Override
    public WidthMode getWidthMode() {
        return widthMode;
    }

    @Override
    public void setEntryW(float width) {
        if (width > 0) {
            entryWidth = width;
        }
    }

    @Override
    public float getEntryW(IEntryRenderer renderer) {
        // If width not defined in Entry renderer - return predefined value
        float optionalW = renderer.getEntryWidth();
        return optionalW != -1 ? optionalW : entryWidth;
    }

    @Override
    public void setEntrySpace(float space) {
        if (space != -1) {
            entrySpace = space;
        }
    }

    @Override
    public float getXAxisHeight() {
        return ChartUtils.convertDpToPixel(36f);
    }

    @Override
    public float getEntrySpace() {
        return entrySpace;
    }

    @Override
    public int getHorizontalPadding() {
        return paddingLeft + paddingRight;
    }

    @Override
    public int getVerticalPadding() {
        return paddingTop + paddingBottom;
    }

    @Override
    public RectF getContentArea() {
        return new RectF(paddingLeft, paddingTop, width - paddingRight, height - paddingBottom);
    }
}

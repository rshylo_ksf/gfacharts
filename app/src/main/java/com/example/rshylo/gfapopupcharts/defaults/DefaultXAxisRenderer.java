package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;

import com.example.rshylo.gfapopupcharts.renderers.IAxisRenderer;
import com.example.rshylo.gfapopupcharts.entries.IAxis;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPort;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public class DefaultXAxisRenderer implements IAxisRenderer {

    private Paint textPaint;
    private TextPaint textPaintInternal;

    public DefaultXAxisRenderer() {
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaintInternal = new TextPaint();
    }

    @Override
    public RectF renderAxis(Canvas c, RectF rect, Entry[] entries, IChartViewPort viewPort,
                            IAxis axis, IEntryRenderer entryRenderer) {
        // Setup
        textPaint.setColor(axis.getTextColor());
        textPaint.setTextSize(axis.getTextSize());
        if (axis.isDrawTextShadow()) {
            textPaint.setShadowLayer(12.0f, 0f, 0f, Color.parseColor("#55000000"));
        }
        textPaintInternal.set(textPaint);
        // Draw
        float translatedX = rect.left;
        float cellW = viewPort.getEntryW(entryRenderer);
        float cellSpace = viewPort.getEntrySpace();
        RectF cell = new RectF(rect);
        for (int i = 0; i < entries.length; i++) {
            Entry e = entries[i];
            cell.left = translatedX;
            cell.right = translatedX + cellW;
            ChartUtils.drawTextInRect(c, textPaintInternal, cell,
                    axis.getValueFormatter().format(e, i)
            );
            translatedX = cell.right + cellSpace;
        }
        return rect;
    }
}

package com.example.rshylo.gfapopupcharts.renderers;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPort;
import com.example.rshylo.gfapopupcharts.interfaces.IChartViewPortOwner;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public abstract class IEntryRenderer {

    private IChartViewPort chart;
    private IEntryValuesRenderer entryValuesRenderer;

    private boolean drawEntryShadow;

    public IEntryRenderer(IEntryValuesRenderer entryValuesRenderer) {
        this.entryValuesRenderer = entryValuesRenderer;
    }

    ///////////////////////////////////////////////////////////////////////////
    // GETTERS / SETTERS
    ///////////////////////////////////////////////////////////////////////////

    public void setChart(IChartViewPortOwner viewPortOwner) {
        this.chart = viewPortOwner.getViewPort();
    }

    public IChartViewPort getChart() {
        return chart;
    }

    public IEntryValuesRenderer getEntryValuesRenderer() {
        return entryValuesRenderer;
    }

    public boolean isDrawEntryShadow() {
        return drawEntryShadow;
    }

    public void setDrawEntryShadow(boolean drawEntryShadow) {
        this.drawEntryShadow = drawEntryShadow;
    }

    ///////////////////////////////////////////////////////////////////////////
    // IMPL.
    ///////////////////////////////////////////////////////////////////////////

    public float getEntryWidth() {
        return -1;
    }

    public final RectF renderEntry(Canvas c, RectF rect, Entry entry,
                                   float pointYSize, int[] stackColors) {
        return renderEntry(c, rect, entry, pointYSize, stackColors, entryValuesRenderer);
    }

    public abstract RectF renderEntry(Canvas c, RectF rect, Entry entry, float pointYSize,
                                      int[] stackColors, IEntryValuesRenderer valuesRenderer);

}

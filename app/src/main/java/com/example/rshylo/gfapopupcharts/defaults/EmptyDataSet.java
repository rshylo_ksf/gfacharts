package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;

import com.example.rshylo.gfapopupcharts.entries.DataSet;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;
import com.example.rshylo.gfapopupcharts.renderers.IEntryValuesRenderer;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/25/2017.
 * whoose.daddy@gmail.com
 */

public class EmptyDataSet extends DataSet {

    private static final float[] emptyValue = new float[]{1f};
    private static final Entry[] emptyEntries = new Entry[]{new Entry(emptyValue, "")};

    public EmptyDataSet() {
        super(emptyEntries, new EmptyDataRenderer());
    }

    private static class EmptyDataRenderer extends IEntryRenderer {

        public static final String EMPTY_MESSAGE = "No available data";

        private Paint textPaint;
        private TextPaint textPaintInternal;
        private int textWidth;

        public EmptyDataRenderer() {
            super(null);
            textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            textPaint.setColor(Color.DKGRAY);
            textPaint.setTextSize(ChartUtils.convertDpToPixel(14f));
            textPaintInternal = new TextPaint(textPaint);
            //
            textWidth = ChartUtils.calcTextSize(textPaint, EMPTY_MESSAGE, null).x;
        }

        @Override
        public RectF renderEntry(Canvas c, RectF rect, Entry entry, float pointYSize, int[] stackColors, IEntryValuesRenderer valuesRenderer) {
            rect.right = rect.left + textWidth;
            ChartUtils.drawTextInRect(c, textPaintInternal, rect, EMPTY_MESSAGE);
            return rect;
        }

        @Override
        public float getEntryWidth() {
            return textWidth;
        }
    }

}

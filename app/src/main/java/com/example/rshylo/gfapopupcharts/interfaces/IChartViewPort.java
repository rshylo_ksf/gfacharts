package com.example.rshylo.gfapopupcharts.interfaces;

import android.graphics.RectF;

import com.example.rshylo.gfapopupcharts.entries.WidthMode;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public interface IChartViewPort {

    void renderSize(IEntryRenderer renderer, int entryCount);

    int getWidth();

    int getHeight();

    void setWidthMode(WidthMode mode);

    WidthMode getWidthMode();

    void setEntryW(float width);

    float getEntryW(IEntryRenderer renderer);

    void setEntrySpace(float space);

    float getEntrySpace();

    float getXAxisHeight();

    int getHorizontalPadding();

    int getVerticalPadding();

    RectF getContentArea();

}

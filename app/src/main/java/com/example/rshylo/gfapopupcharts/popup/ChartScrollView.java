package com.example.rshylo.gfapopupcharts.popup;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;

/**
 * Created by Roman Shylo on 8/23/2017.
 * whoose.daddy@gmail.com
 */

public class ChartScrollView extends HorizontalScrollView {

    public ChartScrollView(Context context) {
        super(context);
        init();
    }

    public ChartScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChartScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return canScroll() && super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return canScroll() && super.onTouchEvent(ev);
    }

    private boolean canScroll() {
        View child = getChildAt(0);
        if (child != null) {
            int childWidth = (child).getWidth();
            return getWidth() < childWidth + getPaddingLeft() + getPaddingRight();
        }
        return false;
    }

}
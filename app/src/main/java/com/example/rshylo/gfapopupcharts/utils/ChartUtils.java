package com.example.rshylo.gfapopupcharts.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public class ChartUtils {

    private static final String TAG = ChartUtils.class.getSimpleName();

    private static DisplayMetrics mMetrics;

    public static void init(Context context) {
        if (mMetrics == null) {
            Resources res = context.getResources();
            mMetrics = res.getDisplayMetrics();
        }
    }

    public static boolean assertInitialization() {
        if (mMetrics == null) {
            throw new IllegalStateException("Utils NOT INITIALIZED." +
                    "You need to call Utils.init(...) in your application class or before using");
        }
        return true;
    }

    private static TextPaint mCalcTextSizePaint = new TextPaint();
    private static StaticLayout mCalcTextSizeSl = null;

    /**
     * calculates the approximate size of a text, depending on a demo text
     * avoid repeated calls (e.g. inside drawing methods)
     */
    public static Point calcTextSize(Paint paint, CharSequence text, @Nullable RectF rect) {
        mCalcTextSizePaint.set(paint);
        int width = rect == null
                ? (int) mCalcTextSizePaint.measureText(text, 0, text.length())
                : (int) rect.width();
        mCalcTextSizeSl = new StaticLayout(text, mCalcTextSizePaint,
                width, Layout.Alignment.ALIGN_CENTER, 1, 1, false);
        return new Point(mCalcTextSizeSl.getWidth(), mCalcTextSizeSl.getHeight());
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device
     * density. NEEDS UTILS TO BE INITIALIZED BEFORE USAGE.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into
     * pixels
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp) {
        assertInitialization();
        return dp * mMetrics.density;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into
     * pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static void drawTextInRect(Canvas canvas, TextPaint textPaint, RectF rect, CharSequence text) {
        canvas.save();
        StaticLayout sl = new StaticLayout(text, textPaint,
                (int) rect.width(), Layout.Alignment.ALIGN_CENTER, 1, 1, false);
        float rectCenter = rect.bottom - ((rect.bottom - rect.top) / 2);
        canvas.translate(rect.left, rectCenter - sl.getHeight() / 2);
        sl.draw(canvas);
        canvas.restore();
    }

}

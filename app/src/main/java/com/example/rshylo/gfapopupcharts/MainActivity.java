package com.example.rshylo.gfapopupcharts;

import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.rshylo.gfapopupcharts.custom.CustomEntryValueFormatter;
import com.example.rshylo.gfapopupcharts.defaults.ColorTemplate;
import com.example.rshylo.gfapopupcharts.defaults.DefaultEntryRenderer;
import com.example.rshylo.gfapopupcharts.defaults.DefaultEntryValuesRenderer;
import com.example.rshylo.gfapopupcharts.defaults.DefaultXAxis;
import com.example.rshylo.gfapopupcharts.defaults.DefaultXAxisFormatter;
import com.example.rshylo.gfapopupcharts.entries.DataSet;
import com.example.rshylo.gfapopupcharts.entries.Entry;
import com.example.rshylo.gfapopupcharts.popup.PopupChartWindow;
import com.example.rshylo.gfapopupcharts.renderers.IEntryRenderer;
import com.example.rshylo.gfapopupcharts.renderers.IEntryValuesRenderer;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

public class MainActivity extends AppCompatActivity {

    DataSet dataSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ChartUtils.init(this);
        Entry[] entries = GfaChartDataFactory.createEntries(TestData.getData().getValuesMap());
        //
        /*DataSet dataSet = new DemoDataSet(new DefaultEntryRenderer(chartView, new CustomEntryValuesRenderer()));
        chartView.setXAxis(new DefaultXAxis());
        chartView.setDataSet(dataSet);*/

        IEntryValuesRenderer entryValuesRenderer = new DefaultEntryValuesRenderer(new CustomEntryValueFormatter());
        entryValuesRenderer.setDrawTextShadow(true);
        IEntryRenderer entryRenderer = new DefaultEntryRenderer(entryValuesRenderer);
        dataSet = new DataSet(entries, entryRenderer, ColorTemplate.getGfaChartColors());
        // Define X Axis
        DefaultXAxisFormatter axisValueFormatter = new DefaultXAxisFormatter();
        axisValueFormatter.setTextStyle(Typeface.BOLD);
        DefaultXAxis xAxis = new DefaultXAxis(axisValueFormatter);
        dataSet.setXAxis(xAxis);

        findViewById(R.id.text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(dataSet);
            }
        });
        getWindow().getDecorView().post(new Runnable() {
            @Override
            public void run() {
                showPopup(dataSet);
            }
        });
    }

    PopupChartWindow window;

    private void showPopup(DataSet dataSet) {
        if (window != null && window.isShowing()) {
            window.dismiss();
            window = null;
        }
        window = new PopupChartWindow(this, dataSet);
        window.show(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (window != null && window.isShowing()) {
            window.onConfigChanges(this);
        }
    }
}

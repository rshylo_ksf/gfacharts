package com.example.rshylo.gfapopupcharts.interfaces;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public interface IChartViewPortOwner {

    IChartViewPort getViewPort();
}

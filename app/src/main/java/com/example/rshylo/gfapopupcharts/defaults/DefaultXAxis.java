package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Color;

import com.example.rshylo.gfapopupcharts.renderers.IAxisRenderer;
import com.example.rshylo.gfapopupcharts.entries.IAxis;
import com.example.rshylo.gfapopupcharts.formatters.IAxisValueFormatter;
import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public class DefaultXAxis extends IAxis {

    public static final float TEXT_SIZE = 10f;

    public DefaultXAxis() {
        this(new DefaultXAxisFormatter());
    }


    public DefaultXAxis(IAxisValueFormatter valueFormatter) {
        this(valueFormatter, new DefaultXAxisRenderer());
    }

    public DefaultXAxis(IAxisValueFormatter valueFormatter, IAxisRenderer axisRenderer) {
        super(valueFormatter, axisRenderer);
        setup();
    }

    private void setup() {
        setTextSize(ChartUtils.convertDpToPixel(TEXT_SIZE));
        setTextColor(Color.BLACK);
        setDrawTextShadow(false);
    }

}

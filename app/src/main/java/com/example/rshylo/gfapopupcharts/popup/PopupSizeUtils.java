package com.example.rshylo.gfapopupcharts.popup;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

import com.example.rshylo.gfapopupcharts.utils.ChartUtils;

/**
 * Created by Roman Shylo on 8/23/2017.
 * whoose.daddy@gmail.com
 */

public class PopupSizeUtils {

    static int getLayoutWidth(Context context) {
        Resources r = context.getResources();
        int screenW = getScreenW(context);
        float resPercent = getStandardWSizePercent(r);
        int padding = 0/*Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16,
                r.getDisplayMetrics()))*/;
        return Math.round(resPercent * screenW) - padding * 2;
    }

    static int getLandscapeSoftBarFix(Context context) {
        if (isLandscape(context) && hasNavigationBar(context)) {
            return Math.round(ChartUtils.convertDpToPixel(48f, context) / 2f);
        }
        return 0;
    }

    private static float getStandardWSizePercent(Resources resources) {
        // Get the percentage as defined by android.R.dimen.dialog_min_width_minor
        TypedValue typedValue = new TypedValue();
        resources.getValue(android.R.dimen.dialog_min_width_minor, typedValue, true);
        return typedValue.getFraction(1, 1);
    }

    private static int getScreenW(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return /*Math.min(size.x, size.y)*/size.x;
    }

    private static boolean hasNavigationBar(Context context) {
        try {
            int id = context.getResources()
                            .getIdentifier("config_showNavigationBar", "bool", "android");
            return id > 0 && context.getResources().getBoolean(id);
        } catch (Exception ignore) {
        }
        return false;
    }

    private static boolean isLandscape(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x > size.y;
    }

}

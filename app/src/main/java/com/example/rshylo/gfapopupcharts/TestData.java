package com.example.rshylo.gfapopupcharts;

import com.example.rshylo.gfapopupcharts.utils.AlphanumComparator;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Roman Shylo on 8/23/2017.
 * whoose.daddy@gmail.com
 */

public class TestData {

    public static final String JSON_MAP = "{\"c25\":\"Jan 2015\",\"c6\":\"138\",\"c19\":\"Nov 2014\",\"c18\":\"123\",\"c39\":\"132\",\"c38\":\"273\",\"c35\":\"398\",\"c34\":\"Apr 2015\",\"c37\":\"May 2015\",\"c10\":\"Aug 2014\",\"c17\":\"252\",\"c16\":\"Oct 2014\",\"c15\":\"125\",\"c14\":\"271\",\"c21\":\"162\",\"c13\":\"Sep 2014\",\"c9\":\"196\",\"c8\":\"429\",\"c12\":\"136\",\"c3\":\"113\",\"c2\":\"398\",\"c1\":\"May 2014\",\"c7\":\"Jul 2014\",\"c11\":\"308\",\"c5\":\"303\",\"c4\":\"Jun 2014\",\"c22\":\"Dec 2014\",\"c23\":\"285\",\"c20\":\"356\",\"c36\":\"136\",\"c26\":\"338\",\"c27\":\"121\",\"c24\":\"133\",\"f\":\"bars_2;var_Digital;bg_2;var_Physical;bg_3;int;$#,##0;$0;-$#,##0\",\"c31\":\"Mar 2015\",\"c28\":\"Feb 2015\",\"c29\":\"398\",\"m\":\"Month by Month report\",\"c30\":\"191\",\"c33\":\"115\",\"c32\":\"262\"}";

    public static PopupChartData getData() {
        Type type = new TypeToken<LinkedTreeMap<String, String>>() {
        }.getType();
        Map<String, String> map = new Gson().fromJson(JSON_MAP, type);
        // Read values
        String title = map.remove("m");
        String format = map.remove("f");
        Map<String, String> sortedMap = new TreeMap<>(new AlphanumComparator());
        sortedMap.putAll(map);
        return new PopupChartData(title, format, sortedMap);
    }

}

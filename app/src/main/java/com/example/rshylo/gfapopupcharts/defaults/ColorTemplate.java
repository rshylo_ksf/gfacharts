package com.example.rshylo.gfapopupcharts.defaults;

import android.graphics.Color;

/**
 * Created by Roman Shylo on 8/21/2017.
 * whoose.daddy@gmail.com
 */

public class ColorTemplate {

    public static final int[] MATERIAL_COLORS = {
            rgb("#2ecc71"), rgb("#f1c40f"), rgb("#e74c3c"),
            rgb("#3498db"), rgb("#512DA8"), rgb("#FF5722")
    };

    public static final int[] GFA_CHART_COLORS = {
            rgb("#000000"), rgb("#d06a01")
    };

    /**
     * Converts the given hex-color-string to rgb.
     */
    public static int rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return Color.rgb(r, g, b);
    }

    public static int[] getDefault() {
        return MATERIAL_COLORS;
    }

    public static int[] getGfaChartColors() {
        return GFA_CHART_COLORS;
    }

}

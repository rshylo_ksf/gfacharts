package com.example.rshylo.gfapopupcharts.entries;

import com.example.rshylo.gfapopupcharts.renderers.IAxisRenderer;
import com.example.rshylo.gfapopupcharts.formatters.IAxisValueFormatter;

/**
 * Created by Roman Shylo on 8/22/2017.
 * whoose.daddy@gmail.com
 */

public abstract class IAxis {

    private IAxisValueFormatter valueFormatter;
    private IAxisRenderer axisRenderer;

    private int textColor;
    private float textSize;
    private boolean drawTextShadow;

    public IAxis(IAxisValueFormatter valueFormatter, IAxisRenderer axisRenderer) {
        this.valueFormatter = valueFormatter;
        this.axisRenderer = axisRenderer;
    }

    ///////////////////////////////////////////////////////////////////////////
    // GETTERS / SETTERS
    ///////////////////////////////////////////////////////////////////////////

    public IAxisValueFormatter getValueFormatter() {
        return valueFormatter;
    }

    public void setValueFormatter(IAxisValueFormatter valueFormatter) {
        this.valueFormatter = valueFormatter;
    }

    public IAxisRenderer getAxisRenderer() {
        return axisRenderer;
    }

    public void setAxisRenderer(IAxisRenderer axisRenderer) {
        this.axisRenderer = axisRenderer;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public boolean isDrawTextShadow() {
        return drawTextShadow;
    }

    public void setDrawTextShadow(boolean drawTextShadow) {
        this.drawTextShadow = drawTextShadow;
    }
}
